#!/bin/bash
IP=`ip addr list enp3s0 | grep "  inet " | head -n 1 \
| cut -d " " -f 6 | cut -d / -f 1`
LOGPATH='/var/log/nfsshtuka';
LOGNAME='nfsshtuka';
DEBUG='yes';
DEST_IP='192.168.1.1';
DEST_PATH='/overlay/shara';
LOCAL_PATH='/mnt';

function main {
  local action;
  action="$1";
  put_log LOG "Nfsshtuka main started! ($action)";
  put_log DEBUG "runlevel is $(get_runlevel)";
  if [[ "$(ismountnfs)" ]]; then
    put_log LOG "Mounted: $DEST_IP:$DEST_PATH on $LOCAL_PATH!";
  else
    put_log ERROR "Not mounted: $DEST_IP:$DEST_PATH on $LOCAL_PATH!";
    mountnfs "$DEST_IP" "$DEST_PATH" "$LOCAL_PATH";
    if [[ "$(ismountnfs)" ]]; then
      put_log LOG "Mounted: $DEST_IP:$DEST_PATH on $LOCAL_PATH!";
      case "$action" in
        "on" )
          put_log LOG "Do action 'ON'.";
          action_on;
          ;;
        "off" )
          put_log LOG "Do action 'OFF'.";
          action_off;
          ;;
        * )
          put_log ERROR "Unknown action!";
          _exit;
          ;;
      esac
    else
      put_log ERROR "Can't mount: $DEST_IP:$DEST_PATH on $LOCAL_PATH!";
      _exit;
    fi
  fi
  put_log LOG "Nfsshtuka main stopped!";
}

function action_on {
  put_log DEBUG "Doing action 'ON'.";
}
function action_off {
  put_log DEBUG "Doing action 'OFF'.";
}

function get_runlevel {
  local tmp runlevel;
  tmp=( $(who -r | tr ' ' '\n') );
  runlevel=${tmp[2]};
  let 'runlevel += 0';
  echo $runlevel;
}

function ismountnfs {
  put_log DEBUG "Check: $DEST_IP:$DEST_PATH on $LOCAL_PATH ...";
  mount | grep  "$DEST_IP:$DEST_PATH on $LOCAL_PATH";
}
function mountnfs {
  put_log DEBUG "Mounting: $DEST_IP:$DEST_PATH on $LOCAL_PATH ...";
  mount -t nfs -O uid=1000,iocharset=utf-8 "$1:$2" "$3";
}

function put_log {
  mkdir "$LOGPATH" 2> /dev/null;
  if [[ "$2" ]]; then
    case "$1" in
      "DEBUG" )
        if [[ "$DEBUG" == "yes" ]]; then
          echo [$(date)]/["$1"]: "$2" >> "$LOGPATH"/"$LOGNAME".log;
        fi
        ;;
      "ERROR" )
        echo [$(date)]/["$1"]: "$2" >> "$LOGPATH"/"$LOGNAME".log;
        ;;
      "LOG" )
        echo [$(date)]/["$1"]: "$2" >> "$LOGPATH"/"$LOGNAME".log;
        ;;
    esac
  fi
}
function _exit {
  put_log ERROR "Unexpected exit!";
  exit 1;
}
main "$@";
put_log LOG "Nfsshtuka exiting...";
